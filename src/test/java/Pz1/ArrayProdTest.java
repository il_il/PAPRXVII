package Pz1;

import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.*;

public class ArrayProdTest {
    @Test
    public void composition() throws Exception {
        ArrayProd arrayProd  = new ArrayProd();
        assertEquals(arrayProd.composition(new int[]{1,2,3}),6);
    }

}