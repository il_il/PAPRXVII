package Pz1.student;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import java.util.ArrayList;
import java.util.ConcurrentModificationException;
import static org.junit.Assert.*;

public class StudentTest {
    static Student student;
    @Before
    public void start(){
        ArrayList<Exam> exams = new ArrayList<Exam>();
        exams.add(new Exam("Kripta", 60,2016,1));
        exams.add(new Exam("TPRO",60, 2016,1));
        exams.add(new Exam("English",60, 2016,1));
        student = new Student("Maksim", "Zaika", new Group("UIB-15-1", "TKVT"), exams);
    }
    @Test
    public void testMaxMark() throws Exception {
        assertEquals(student.maxMark(), 98);
    }
    @Test
    public void testAddMark() throws Exception {
        student.addMark("Kripta", 100);
        assertEquals(student.exams.get(0).mark, 100);
    }
    @Test(expected = ConcurrentModificationException.class)
    public void testDellayMark() throws Exception {
        student.dellayMark("Kripta");
        assertEquals(0,student.exams.get(0).mark);
    }
    @Test
    public void testNumberOfExaminations() throws Exception {

        assertEquals(student.numberOfExaminations(60),2);
    }
    @Test
    public void testaverageScore(){
        assertEquals(student.averageScore(),60);
    }

}