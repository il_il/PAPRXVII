package Pz1;

import com.sun.xml.internal.ws.policy.spi.AssertionCreationException;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import static org.junit.Assert.*;

@Suite.SuiteClasses({ArrayProdTest.class})
public class ArraySumTest {

    static  ArraySum arraySum1 = new ArraySum(new int[]{1,2,3,4});
    @Test
    public void getMas() throws Exception {
    }

    @Test
    public void setMas() throws Exception {
    }

    @Test
    public void sum() {
        ArraySum arraySum = new ArraySum();
        assertEquals(arraySum.sum(new int[]{1,2,3,4,5}),15);
    }

    @Test
    public void sum1() throws Exception {
        assertEquals(arraySum1.sum(),10);
    }
    @Test(expected = NullPointerException.class)
    public void sum2()throws Exception{
        arraySum1.sum(null);
    }

}