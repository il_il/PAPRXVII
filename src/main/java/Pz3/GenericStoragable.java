package Pz3;

import lombok.Getter;
import lombok.Setter;

import java.util.*;

import static javafx.scene.input.KeyCode.T;

/**
 * Created by Давид on 07.02.2017.
 */
public interface GenericStoragable <K extends Number, V> {
    K put(V value);
    V delete (K id);
    boolean upload (K id, V value);
    V read (K id);

    @Getter
    @Setter
    class Node <K, V> {
        public Node(K id, V value) {
            this.id = id;
            this.value = value;
        }
        private K id;
        private V value;
    }
}

