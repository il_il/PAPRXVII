package Pz3;

import java.util.ArrayList;
public class GenericStoragage<V extends Entity<Integer>> implements GenericStoragable<Integer, V> {

    private ArrayList<Node<Integer, V>> list = new ArrayList<Node<Integer, V>>();
    private int id = 0;

    public Integer put(V value) {
        list.add(new Node<Integer, V>(id, value));
        return id++;
    }

    public V delete(Integer id) {
        id = getIdOfArray(id);
        V result = list.get(id).getValue();
        list.remove(id);
        return result;
    }

    public boolean upload(Integer id, V value){
        if(getIdOfArray(id) == -1) {
            list.set(getIdOfArray(id), new Node<Integer, V>(id, value));
            return true;
        }else return false;
    }

    public V read(Integer id) {
        return list.get(getIdOfArray(id)).getValue();
    }

    private int getIdOfArray(int id){
        for (Node<Integer, V> element:list)
            if (element.getId() == id)
                return list.indexOf(element);
        return -1;
    }
}

