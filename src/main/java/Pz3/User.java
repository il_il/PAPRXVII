package Pz3;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;

@Getter
@Setter
public class User {
    private String name;
    private String surname;
    private String login;
    private String pass;
    ArrayList<Ticket> tickets;
    {
        tickets = new ArrayList<Ticket>();
    }

}
