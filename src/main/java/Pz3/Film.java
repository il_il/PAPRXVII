package Pz3;

import java.util.ArrayList;

public class Film {
    String name;
    String description;
    ArrayList<Session> sessions  = new ArrayList<Session>();

    public Film(String name, String description, ArrayList<Session> sessions) {
        this.name = name;
        this.description = description;
        this.sessions = sessions;
    }
}
