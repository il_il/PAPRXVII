package Homework3;

import java.util.ArrayList;

public class GenericStorage<T> {
    private ArrayList<T>  arrayList;
    private int lastFreeIndex = 0;
    public GenericStorage() {
        arrayList = new ArrayList<T>(10);
    }
    public GenericStorage(int size) {
        arrayList = new ArrayList<T>(size);
    }
    public void add (T obj){
        arrayList.add(lastFreeIndex++, obj);
    }
    public T get(int index){
        checkError(index);
        return arrayList.get(index);
    }
    T[] getAll(){
        return (T[]) arrayList.toArray();
    }
    boolean update(int index, T obj){
        checkError(index);
        if (arrayList.get(index)==null)
            arrayList.set(index,obj);
        else return false;
        return true;
    }
    void delete(int index){
        checkError(index);
        arrayList.remove(index);
        lastFreeIndex--;
    }
    void delete(T obj){
        arrayList.remove(obj);
        lastFreeIndex--;
    }
    T[] getArrayWithoutNullElements(){
        ArrayList<T> result = new ArrayList<T>();
        for (T element:arrayList)
            if (element==null)
                result.add(element);
        return (T[]) arrayList.toArray();
    }

    void checkError(int index) throws IndexOutOfBoundsException{
        if (index>=lastFreeIndex)
            throw new IndexOutOfBoundsException();
    }
}

