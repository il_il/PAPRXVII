package Pz2;
import java.io.IOException;

public class Main {
    public static void main(String[] args) throws IOException {
        ParserRate parserRate = new ParserRate();
        for(ParserRate.Rate rate : parserRate.parse("baby2008.html", "utf-8", "<td>(?<index>\\d+)</td><td>(?<woman>.+?)</td><td>(?<man>.+?)</td>"))
            System.out.println(rate.toString());
    }
}
