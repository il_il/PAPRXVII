package Pz2;

import lombok.Data;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ParserRate {

    public ArrayList<Rate> parse(String path, String encoding, String regular) throws IOException {
        BufferedReader br = new BufferedReader( new InputStreamReader(new FileInputStream(path),encoding));
        String s = null;
        StringBuilder sb = new StringBuilder();
        while ((s = br.readLine()) != null) {
            sb.append(s).append(System.getProperty("line.separator"));
        }
        Pattern pattern = Pattern.compile(regular);
        Matcher matcher = pattern.matcher(sb.toString());
        ArrayList<Rate> result = new ArrayList<Rate>();
        while (matcher.find()){
            result.add(new Rate(Integer.parseInt(matcher.group("index")),matcher.group("man"),matcher.group("woman")));
        }
        return result;
    }


    @Data
    class Rate {
        public Rate(int index, String man, String woman) {
            this.index = index;
            this.man = man;
            this.woman = woman;
        }
        int index;
        String man;
        String woman;

    }
}