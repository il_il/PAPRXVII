package Homework6.part2;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class App {
    public static void main(String[] args) {
        try(Scanner sc =  new Scanner(System.in)){
            for (String  request = sc.next(); !request.equals("exit"); request = sc.next()){
                switch(request){
                    case "touch":
                        try{
                            if (new File(sc.nextLine().replaceFirst(" ", "")).createNewFile())
                                System.out.println("Successful.");
                            else
                                System.out.println("Error!");
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        break;
                    case "rm":
                        if (new File(sc.nextLine().replaceFirst(" ", "")).delete())
                            System.out.println("Successful.");
                        else
                            System.out.println("Error!");
                        break;
                    case "rename":
                        Matcher matcher = Pattern.compile("(?<path>\\D+) (?<name>[A-я]+)").matcher(sc.nextLine().replaceFirst(" ", ""));
                        File renameFile;
                        if (matcher.find() && (renameFile = new File(matcher.group("path"))).exists())
                            if (renameFile.renameTo(new File(renameFile.getParent() + "\\" +  matcher.group("name"))))
                                System.out.println("Successful.");
                            else
                                System.out.println("Error rename!");
                        else
                            System.out.println("Error, file not exists!");

                        break;
                    case "ls":
                        File dir;
                        if ((dir = new File(sc.nextLine().replaceFirst(" ", ""))).isDirectory())
                            for (File file : dir.listFiles())
                                System.out.println(file.getName());
                        else
                            System.out.println("Error, it's not directory!");
                        break;
                    default:
                        System.out.println("Error command!");
                }
            }
        }
    }
}

