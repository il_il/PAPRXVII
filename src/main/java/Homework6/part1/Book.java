package Homework6.part1;
import lombok.Getter;
import lombok.ToString;

import java.io.*;

@Getter
@ToString
public class Book implements Serializable {
    String title;
    String author;
    int year;
    public Book(String[] param) {
        this.title = param[0];
        this.author = param[1];
        this.year = Integer.parseInt(param[2]);
    }
    public Book(String title, String author, int year) {
        this.title = title;
        this.author = author;
        this.year = year;
    }

    void addBookToFile(BufferedWriter bw) throws IOException {
        bw.write(getTitle() + getAuthor() + getYear());
    }

    void addBookObjectToFile(OutputStream os) throws IOException {
        try (ObjectOutputStream oos = new ObjectOutputStream(os)) {
            oos.writeObject(this);
        }
    }

}

