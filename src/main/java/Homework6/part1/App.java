package Homework6.part1;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class App {
    public static void main(String[] args) throws IOException {
        try(BufferedReader br = new BufferedReader(new FileReader("CollectionBook"))){
            String s;
            List<Book> list = new ArrayList<>();
            while ((s = br.readLine()) != null) {
                list.add(new Book(s.split(";")));
            }
            System.out.println(list);
        }
    }
}
