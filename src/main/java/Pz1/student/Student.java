package Pz1.student;

import java.util.ArrayList;

public class Student {
    String name;
    String surnmame;
    Group group;
    public ArrayList<Exam>exams;

    public Student(String name, String surnmame, Group group, ArrayList<Exam> exams) {
        this.name = name;
        this.surnmame = surnmame;
        this.group = group;
        this.exams = exams;
    }

    public int maxMark(){

        int max = 0;
        for (Exam exam : exams) {
            if(max < exam.getMark()){
                max = exam.getMark();
            }
        }
        return max;
    }
    public void addMark(String nameExam, int mark) throws Exception {
        proverka(nameExam);
        for (Exam exam:exams) {
            if(nameExam == exam.getSubject()){
                exam.setMark(mark);

            }
            exam.getSubject();
        }
    }
    public void dellayMark(String nameExam) throws Exception {
        proverka(nameExam);
        Exam thisExam = null;
        for(Exam exam:exams){
            if(nameExam == exam.getSubject()){
                exams.remove(exam);
            }
            exam.getSubject();
        }

    }

    public boolean proverka(String nameExam) throws Exception {
        Exam thisExam = null;
        for (Exam exam : exams) {
            if (exam.getSubject() == nameExam) {
                thisExam = exam;
                break;
            }
        }
        if (thisExam == null) throw new Exception("Error");
        return thisExam.getMark() >= 0;
    }

    public int numberOfExaminations(int mark) throws Exception {
        int count=0;
        for(Exam exam :exams){
            if(mark == exam.getMark()){
                count++;
            }
        }
        return count;
    }
    public int averageScore(){
        int avaragescore = 0;
        int buffer=0;
        int counter=0;
        for(Exam exam :exams){
            buffer +=exam.getMark();
            counter++;
        }
        return buffer/counter;
    }
}

