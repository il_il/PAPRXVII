package Pz1.student;
import lombok.Data;
@Data
public class Exam {
    String subject;
    int mark;
    Date date;

    public Exam(String subject, int mark, int year , int semestr) {
        this.subject = subject;
        this.mark = mark;
        date = new Date(year, semestr);
    }

    @Data
    public class Date {
        private int year;
        private int semestr;

        public Date(int year, int semestr) {
            this.year = year;
            this.semestr = semestr;
        }
    }

    public String getSubject() {
        return subject;
    }

    public int getMark() {
        return mark;
    }

    public Date getDate() {
        return date;
    }

    public void setMark(int mark) {
        this.mark = mark;
    }
}
