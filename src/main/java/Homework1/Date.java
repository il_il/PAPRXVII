package Homework1;

public class Date {
    public class Year{
        private int year;
        private boolean leapYear;
        Year(int year){
            this.year = year;
            if(year%2 == 0 && year%4 == 0){
                leapYear = true;
            }
            else{
                leapYear = false;
            }
        }

        public int getYear() {
            return year;
        }
        public boolean getLeapYear(){
            return leapYear;
        }
    }

    public class Month{
        private int month;
        private int getDays;
        Month(int month){
            this.month = month;
        }

        public int getDays(int monthNumber, boolean leapYear){
            switch(monthNumber){
                case 1: getDays=30;break;
                case 2: if(leapYear == true){getDays = 29;} else {getDays=28;}break;
                case 3: getDays=31;break;
                case 4: getDays=30;break;
                case 5: getDays=31;break;
                case 6: getDays=30;break;
                case 7: getDays=31;break;
                case 8: getDays=31;break;
                case 9: getDays=30;break;
                case 10:getDays=31;break;
                case 11:getDays=30;break;
                case 12:getDays=31;break;
                default:
                    System.out.println("Error");break;
            }
            return getDays;
        }

        public int getMonth() {
            return month;
        }
    }
    public class Day{
        private int day;
        Day(int day){
            this.day = day;
        }

        public int getDay() {
            return day;
        }
    }

    private int day;
    private int month;
    private int year;
    private int dayBetween;;
    private  Year objYear;
    private Month objMonth;
    private Day objDay;
    private DayOfWeek dayOfWeek;


    Date(int day, int month, int year){
        this.day = (objDay = new Day(day)).getDay();
        this.month = (objMonth = new Month(month)).getMonth();
        this.year = (objYear = new Year(year)).getYear();
        int a = (14 - month) / 12, y = year - a, m = month + 12 * a - 2;
        dayOfWeek = DayOfWeek.valueOf((7000 + (day + y + y / 4 - y / 100 + y / 400 + (31 * m) / 12) - 1) % 7);
        System.out.print(getDayOfWeek().toString());
    }

    public DayOfWeek getDayOfWeek() {
        return dayOfWeek;
    }
    public int getDayOfYear(){
        int result = 0;
        Month month1 = new Month(1);
        result += month1.getDays(objMonth.getMonth(), objYear.getLeapYear()) + getDay();
        return result;}

    public int daysBetween(Date date){
        if(day > date.day){
            dayBetween = day - date.day;
        }
        else{
            if(day<date.day){
                dayBetween = date.day - day;
            }
        }
        return dayBetween;
    }

    public Year getObjYear() {
        return objYear;
    }

    public Month getObjMonth() {
        return objMonth;
    }

    public Day getObjDay() {
        return objDay;
    }

    public int getDay() {
        return day;
    }

    public int getMonth() {
        return month;
    }

    public int getYear() {
        return year;
    }

    @Override
    public String toString() {
        return "Date{" +
                "day=" + day +
                ", month=" + month +
                ", year=" + year +
                '}';
    }
}
