package Homework7;


public class App2 {
    public static void main(String[] args) {
        final Test test1 = new Test();
        final Test test2 = new Test();
        Thread thread1 = new Thread(() -> test1.hello(test2));
        Thread thread2 = new Thread(() -> test2.hello(test1));
        thread1.start();
        thread2.start();
    }
}

class Test {
    synchronized void hello(Test test) {
        System.out.println(); //synchronized point
        test.hello2();
    }
    synchronized void hello2() {
    }
}
