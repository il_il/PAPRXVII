package Homework7;
public class App3 {
    public static void main(String[] args) {

        new Thread(Timer::count).start();
        new Thread(Timer::count).start();
    }
}

class Timer {
    static int count1 = 0;
    static int count2 = 0;
    Timer timer = this;

    static void count() {
        while (true) {
            add();
        }
    }

    /*synchronized*/ static void add(){
        System.out.println(Thread.currentThread() + " - " + (count1 == count2));
        count1++;
        try {
            Thread.sleep(10);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        count2++;
    }

}