package Homework7;


public class App1 {
    public static void main(String[] args) {
        new Thread(()-> {
            for (;;) {
                System.out.println(System.currentTimeMillis());
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();
        new ThreadTest().start();
    }
}

class ThreadTest extends Thread{
    @Override
    public void run() {
        for (;;) {
            System.out.println(System.currentTimeMillis());
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
