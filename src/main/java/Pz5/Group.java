package Pz5;

import es.hol.dvininc.oracle.academic.pr.pr4.Student;

import java.io.Serializable;
import java.sql.Array;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Group implements Serializable{
    List<Student> students;

    Group(Student... students){
        this.students = new ArrayList<>();
        Collections.addAll(this.students, students);
    }

}
