package Pz5;

import es.hol.dvininc.oracle.academic.pr.pr4.Student;

import java.io.*;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static es.hol.dvininc.oracle.academic.dz.dz2.StringUtils.change_words_in_sentences;

public class FileUtils {
    private static void write(String path, String s, String encode) {
        try (BufferedWriter bw = new BufferedWriter(new FileWriter(new File(path)))) {
            bw.write(s);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        randomNumbersInFile("randomNumbers", -50, 80, 30);
        sortNumbersInFile("randomNumbers");
        printStudentWith90("randomNumbers");
        changeWordsInSentences("SomeText");
        //bufferedCopyFile("SomeText", "SomeTextCopy");
        copyFile("SomeText", "SomeTextCopy");
        Group group = new Group(new Student("David", "Dvinskih", 1),  new Student("Nastya", "Pavlushenko", 1), new Student("Dasha", "Osipova", 1));
        try(ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(new  File("ObjectInFile")))) {
            oos.writeObject(group);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static String read(String path) {
        try (BufferedReader br = new BufferedReader(new FileReader(new File(path)))) {
            String s = null;
            StringBuilder sb = new StringBuilder();
            while ((s = br.readLine()) != null) {
                sb.append(s).append(System.getProperty("line.separator"));
            }
            return sb.toString();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    protected static void randomNumbersInFile(String path, int min, int max, int amount) {
        int[] array = new int[amount];
        for (int i = 0; i < amount; i++)
            array[i] = (int) (Math.random() * (Math.abs(max - min)) - Math.abs(min));
        String s = "";
        for (int i : array)
            s += i + "\n";
        write(path, s, "utf-8");
    }

    protected static void sortNumbersInFile(String path) {
        try (BufferedReader br = new BufferedReader(new FileReader(new File(path)))) {
            List<Integer> list = new ArrayList<>();
            String s = null;
            while ((s = br.readLine()) != null) {
                list.add(Integer.parseInt(s));
            }
            list.sort((Comparator.comparingInt(o -> o)));
            String result = "";
            for (Integer integer : list)
                result += String.valueOf(integer) + "\n";
            write("randomNumbers", result, "utf-8");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    protected static void printStudentWith90(String path) {
        String text = read("StudentsWith90");
        Matcher matcher = Pattern.compile("(?<name>.+?) = (?<mark>\\d+)").matcher(text);
        Map<String, List<Integer>> map = new HashMap<>();
        while (matcher.find()) {
            List<Integer> temp;
            if (map.get(matcher.group("name")) != null)
                temp = new ArrayList<>(map.get(matcher.group("name")));
            else
                temp = new ArrayList<>();
            temp.add(Integer.valueOf(matcher.group("mark")));
            map.put(matcher.group("name"), temp);
        }
        map.forEach((o1, o2) -> {
            int count = o2.size();
            int sum = 0;
            for (Integer integer : o2) sum += integer;
            if (sum / count >= 90)
                System.out.println(o1);
        });

    }

    protected static void changeWordsInSentences(String path) {
        write(path, change_words_in_sentences(read(path)), "utf-8");
    }

    protected static void bufferedCopyFile(String from, String to)  {
        try (
                BufferedWriter bw = new BufferedWriter(new FileWriter(new File(to)));
                BufferedReader br = new BufferedReader(new FileReader(new File(from)))
        ) {
            String s = null;
            while ((s = br.readLine()) != null) {
                bw.write(s + "\n");
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void copyFile(String from, String to)  {
        try(
                FileInputStream fr = new FileInputStream(from);
                FileOutputStream fw =new FileOutputStream(new File(to))
        ){
            int i;
            while((i = fr.read())!= -1)
                fw.write(i);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
