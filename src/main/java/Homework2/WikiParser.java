package Homework2;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
public class WikiParser {
    public static String parse(String s){
        Pattern pattern;
        Matcher matcher;
        pattern = Pattern.compile("((?<prefix>(^|\n))(?<text>[^#\n]+))");
        matcher = pattern.matcher(s);
        s = matcher.replaceAll("${prefix}<p>${text}</p>");
        for (int i= 1; i<=6; i++){
            pattern = Pattern.compile("(?<prefix>^|\n)[#]{" + i + "}(?<text>[^#\n]+)");
            matcher = pattern.matcher(s);
            s = matcher.replaceAll("${prefix}<h" + i + ">${text}</h" + i + ">");
        }
        pattern = Pattern.compile("\\*{2}(?<text>[^\\W]+?)\\*{2}");
        matcher = pattern.matcher(s);
        s = matcher.replaceAll("<strong>${text}</strong>");
        pattern = Pattern.compile("\\*(?<text>[^\\W]+?)\\*");
        matcher = pattern.matcher(s);
        s = matcher.replaceAll("<em>${text}</em>");
        pattern = Pattern.compile("\\[(?<text>.+?)\\]\\((?<link>.+?)\\)");
        matcher = pattern.matcher(s);
        s = matcher.replaceAll("<a href=“${link}“>${text}</a>");
        s = "<html>\n" +
                "<body>\n" + s +
                "\n</body>\n" +
                "</html>";
        System.out.println(s);
        return s;
    }
}
