package Homework2;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
public class StringUtils {
    public String turn_string(String s) {
        StringBuilder result = new StringBuilder();
        for (int i = s.length() - 1; i >= 0; i--) result.append(s.toCharArray()[i]);
        return result.toString();
    }

    public boolean palindrome(String s) {
        s = s.replaceAll(" ", "");
        int len = s.length() / 2;
        StringBuilder s1 = new StringBuilder(), s2 = new StringBuilder();
        for (int i = 0, j = s.length() - 1; (i < len) && (j >= len); i++, j--) {
            s1.append(s.toCharArray()[i]);
            s2.append(s.toCharArray()[j]);
        }
        return s1.toString().equalsIgnoreCase(s2.toString());
    }

    public String check_length(String s) {
        StringBuilder result = new StringBuilder();
        if (s.length() > 10) {
            for (int i = 0; i < 6; i++)
                result.append(s.toCharArray()[i]);
        } else {
            result.append(s);
            for (int i = s.length() - 1; i < 11; i++)
                result.append('o');
        }
        return result.toString();
    }

    public static String change_words(String s) {
        String[] s_words = s.split("[.,:;!?() ]+");
        Pattern pattern = Pattern.compile("^([.,:;?!() ]*)[^.,:;?!() ]+");
        Matcher matcher = pattern.matcher(s);
        s = matcher.replaceFirst("$1" + s_words[s_words.length - 1]);
        pattern = Pattern.compile("[^.,:;?!() ]+([.,:;?!() ]+)$");
        matcher = pattern.matcher(s);
        if (!new String().equals(s_words[0])) {
            s = matcher.replaceFirst(s_words[0] + "$1");
        } else if (s_words.length > 1) {
            s = matcher.replaceFirst(s_words[1] + "$1");
        }
        return s;
    }

    public static String change_words_in_sentences(String s) {
        StringBuffer result = new StringBuffer();
        String[] s_sentences = s.split("\\.");
        for (String sentences : s_sentences) {
            result.append(change_words(sentences + "."));
        }
        return result.toString();
    }

    public boolean hasABC(String s) {
        char[] s_char = s.toCharArray();
        for (char ch : s_char) if ((ch != 'a') && (ch != 'b') && (ch != 'c')) return false;
        return true;
    }

    public boolean hasDate(String s) {
        return Pattern.compile("((0?\\d)|(1[012]))\\.((0?\\d)|([12]\\d)|(3[01]))\\.(\\d{2})").matcher(s).matches();
    }

    public boolean hasPost(String s) {
        return Pattern.compile("\\d{5}").matcher(s).matches();
    }

    public ArrayList<String> arrayOfTel(String s) {
        Pattern pattern = Pattern.compile("\\+\\d\\(\\d{3}\\)\\d{3}-\\d{2}-\\d{2}");
        Matcher matcher = pattern.matcher(s);
        ArrayList<String> list = new ArrayList<String>();
        while (matcher.find()) {
            list.add(matcher.group());
        }
        return list;
    }
}
