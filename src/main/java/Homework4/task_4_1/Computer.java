package Homework4.task_4_1;

import lombok.Getter;

import java.util.Arrays;
import lombok.Getter;
import java.util.Arrays;
import java.util.Comparator;
import java.util.concurrent.CompletableFuture;


@Getter
public class Computer implements Comparable<Computer>{
    public Computer(String company, String model, int year) {
        this.company = company;
        this.model = model;
        this.year = year;
    }

    String company;
    String model;
    int year;

    @Override
    public int compareTo(Computer o) {
        if (getCompany().compareTo(o.getCompany())!=0)
            return getCompany().compareTo(o.getCompany());
        else if (getModel().compareTo(o.getModel())!=0)
            return getModel().compareTo(o.getModel());
        else return getYear()-o.getYear();
    }

    static <T extends Comparable> int maxValueInCompareTo(T computers[]){
        final int[] max = {0};
        final int[] temp = new int[1];
        Arrays.sort(computers, (o1, o2)->{
            temp[0] = o1.compareTo(o2);
            if (max[0] < temp[0]) max[0] = temp[0];
            return temp[0];
        });
        return max[0];
    }

    public static void main(String[] args) {
        Computer[] computers = {
                new Computer("ASUS", "K50C", 2009),
                new Computer("Lenovo", "MIIX10", 2009),
                new Computer("DELL", "K50C", 2002),
                new Computer("Xiaomi", "Redmi S3", 2015),
                new Computer("ASUS", "K50C", 2016),
        };
        System.out.println(maxValueInCompareTo(computers));

        System.out.println(maxValueInCompareTo(new Integer[]{66, 0}));
        System.out.println(maxValueInCompareTo(new String[]{"a", "d","b"}));

    }
}
