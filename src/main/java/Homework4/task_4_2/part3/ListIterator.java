package Homework4.task_4_2.part3;

import java.util.Iterator;

public interface ListIterator<E> extends Iterator<E> {
    boolean hasPrevious();
    E previous();
    void set(E e);
    void remove();
}
