package Homework4.task_4_2.part3;

public interface ListIterable<E> {
    ListIterator<E> listIterator();
}
