package Homework4.task_4_2.part1;

import java.util.Arrays;

public class Demo {
    public static void main(String[] args) {
        MyDeque<Number> deque = new MyDequeImpl<>();
        deque.addLast(8.88);
        System.out.println(deque);
        deque.addFirst(433);

        System.out.println("list contains 433 --> " + deque.contains(433));
        System.out.println(deque);
        System.out.println(Arrays.toString(deque.toArray()));
        System.out.println(deque.getFirst());
        System.out.println(deque.getLast());
        deque.addFirst(55);
        deque.addLast(34);
        System.out.println(deque.removeFirst());
        System.out.println(deque);
        System.out.println(deque.removeFirst());
        deque.addFirst(55);
        deque.addLast(34);
        System.out.println(deque);
        deque.addFirst(55);
        deque.addLast(34);
        System.out.println(deque.removeFirst());
        System.out.println(deque);
        System.out.println(deque.containsAll(deque));
        System.out.println(deque.contains(1));
        System.out.println(deque.contains(34));
    }
}