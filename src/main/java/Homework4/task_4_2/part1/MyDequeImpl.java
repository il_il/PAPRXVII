package Homework4.task_4_2.part1;


import Homework4.task_4_2.part1.MyDeque;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

public class MyDequeImpl<E> implements MyDeque<E> {
    private Node<E>[] array;
    private int nextIndex;
    private int length;
    private Node<E> firstNode;
    private Node<E> lastNode;

    public MyDequeImpl() {
        initialization();
    }

    private void initialization() {
        this.array = new Node[2];
        this.nextIndex = 0;
        this.length = 0;
        this.firstNode = null;
        this.lastNode = null;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("{");
        for (E e : (E[]) toArray()) {
            sb.append(e);
            sb.append(", ");
        }
        if (!sb.toString().equals("{"))
            sb.delete(sb.length() - 2, sb.length());
        sb.append("}");
        return sb.toString();
    }

    @Override
    public void addFirst(E e) {
        if (array.length == nextIndex)
            enlarge();
        array[nextIndex] = new Node<>(e, null, null);
        if (firstNode != null) {
            array[nextIndex].setNext(firstNode);
            firstNode.setPrev(array[nextIndex]);
        } else if (lastNode != null) {
            array[nextIndex].setNext(lastNode);
            lastNode.setPrev(array[nextIndex]);
        }
        firstNode = array[nextIndex++];
        if (lastNode == null)
            lastNode = firstNode;
        length++;
    }

    @Override
    public void addLast(E e) {
        if (array.length == nextIndex)
            enlarge();
        array[nextIndex] = new Node<>(e, null, null);
        if (lastNode != null) {
            array[nextIndex].setPrev(lastNode);
            lastNode.setNext(array[nextIndex]);
        } else if (firstNode != null) {
            array[nextIndex].setPrev(firstNode);
            firstNode.setNext(array[nextIndex]);
        }
        lastNode = array[nextIndex++];
        if (firstNode == null)
            firstNode = lastNode;
        length++;
    }

    @Override
    public E removeFirst() {
        Node<E> result = firstNode;
        firstNode = firstNode.getNext();
        removeInArray(result);
        return result.getElement();
    }

    @Override
    public E removeLast() {
        Node<E> result = lastNode;
        lastNode = lastNode.getPrev();
        removeInArray(result);
        return result.getElement();
    }

    @Override
    public E getFirst() {
        return firstNode.getElement();
    }

    @Override
    public E getLast() {
        return lastNode.getElement();
    }

    @Override
    public boolean contains(Object o) {
        for (Object o1 : toArray())
            if (o.equals(o1))
                return true;
        return false;
    }

    @Override
    public void clear() {
        initialization();
    }

    @Override
    public Object[] toArray() {
        Object[] result = new Object[length];
        Node<E> node = firstNode;
        result[0] = firstNode.getElement();
        for (int i = 1; (node = node.getNext()) != null; i++)
            result[i] = node.getElement();
        return result;
    }


    @Override
    public int size() {
        return length;
    }

    @Override
    public boolean containsAll(MyDeque<? extends E> deque) {
        for (Object o : deque.toArray())
            if (!contains(o))
                return false;

        return true;
    }

    private void enlarge() {
        int newLength = length * 2;
        Node<E>[] newArray = new Node[newLength];
        int used = 0;
        for (int i = 0; i < nextIndex; i++)
            if (array[i] != null)
                newArray[used++] = array[i];
        array = newArray;
        nextIndex = used;
    }

    private void removeInArray(Node<E> e) {
        for (int i = 0; i < array.length; i++)
            if (array[i] == e) {
                array[i] = null;
                length--;
                break;
            }
    }

    @Getter
    @Setter
    @ToString
    private static class Node<E> {
        // хранимый элемент
        private E element;
        // ссылка на следующий элемент списка
        private Node<E> next;
        // ссылка на предыдущий элемент списка
        private Node<E> prev;

        Node(E element, Node<E> next, Node<E> prev) {
            setElement(element);
            setNext(next);
            setPrev(prev);
        }
    }
