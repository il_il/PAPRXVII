package Homework4.task_4_2.part2;

import java.util.Arrays;
import java.util.Iterator;

@SuppressWarnings("Duplicates")
public class Demo {
    public static void main(String[] args) {
        MyDeque<Number> deque = new MyDequeImpl<>();
        deque.addLast(8.88);
        System.out.println(deque);
        deque.addFirst(433);

        System.out.println("list contains 433 --> " + deque.contains(433));
        System.out.println(deque);
        System.out.println(Arrays.toString(deque.toArray()));
        System.out.println(deque.getFirst());
        System.out.println(deque.getLast());
        deque.addFirst(55);
        deque.addLast(34);
        System.out.println(deque.removeFirst());
        System.out.println(deque);
        System.out.println(deque.removeLast());
        deque.addFirst(55);
        deque.addLast(34);
        System.out.println(deque);
        deque.addFirst(55);
        deque.addLast(34);
        System.out.println(deque.removeFirst());
        System.out.println(deque);
        System.out.println(deque.containsAll(deque));
        System.out.println(deque.contains(1));
        System.out.println(deque.contains(34));
        for (Number number : deque) {
            System.out.println(number);
        }
        Iterator<Number> it = deque.iterator();
        while(it.hasNext()){
            System.out.println(it.next());
            it.remove();
        }
        for (Number number : deque) {
            System.out.println("deque  = " + number);
        }
        System.out.println(deque);
        deque.addFirst(55);
        deque.addLast(34);
        it = deque.iterator();
        while(it.hasNext())
            System.out.println(it.next());
        System.out.println("//////////");
        deque.forEach(System.out::println);
    }
}