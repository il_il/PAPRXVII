package Homework5;


import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Scanner;
import java.util.regex.Pattern;

public class App{
    public static void main(String[] args) throws IOException {
        try(Scanner sc = new Scanner(System.in)){
            System.out.print("Enter path to directory with vocabularies: ");
            String pathVocabularies = sc.nextLine();
            while(!new File(pathVocabularies).exists()){
                System.out.print("Error! Path doesn't exists, please repeat:x");
                pathVocabularies = sc.nextLine();
            }
            File[] vocabularies = new File(pathVocabularies).listFiles((dir, name) -> Pattern.compile("[A-Z]+-[A-Z]+.txt").matcher(name).find());
            StringBuilder out = new StringBuilder();
            HashMap<String, HashMap<String,String>> mapVocabularies = new HashMap<>();
            for (File vocabulary : vocabularies) {
                String temp;
                out.append((temp = vocabulary.getName().replace(".txt", "")) + " ");
                mapVocabularies.put(temp, Translate.mapVocabulary(vocabulary.getAbsolutePath()));
            }
            System.out.print("Choose type of translate ( " + out + "): ");
            String howTranslate = sc.nextLine();
            if(checkForEqualsFiles(vocabularies, howTranslate)) {
                System.out.print("Enter path to file, that need to translate: ");
                String fileThatTranslate = sc.nextLine();
                while(!new File(fileThatTranslate).exists()){
                    System.out.print("Error! Path doesn't exists, please repeat:");
                    fileThatTranslate = sc.nextLine();
                }
                System.out.println(Translate.translate(fileThatTranslate, mapVocabularies.get(howTranslate)));
            }
        }
    }

    private static boolean checkForEqualsFiles(File[] files, String s){
        for (File file : files)
            if (file.getName().contains(s))
                return true;
        throw new IllegalStateException();
    }
}