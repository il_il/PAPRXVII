package Homework5;

import java.io.*;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Translate {

    public static HashMap<String, String> mapVocabulary(String path) throws IOException {
        HashMap<String, String> result = new HashMap<String, String>();
        try(BufferedReader fileVocabulary = new BufferedReader(new InputStreamReader(new FileInputStream(new File(path)), "windows-1251"))){
            String s;
            String[] sa;
            while ((s = fileVocabulary.readLine()) != null) {
                sa = s.split(";-;");
                result.put(sa[0], sa[1]);
            }
        }
        return result;
    }

    public static String translate(String path, HashMap<String, String> vocabulary) throws IOException {
        try(BufferedReader file = new BufferedReader(new FileReader(path))){
            StringBuilder sb = new StringBuilder();
            String s;
            while ((s = file.readLine()) != null)
                sb.append(s).append("\n");
            Matcher matcher = Pattern.compile("[A-я]+").matcher(sb);
            StringBuffer result = new StringBuffer();
            String toReply;
            while (matcher.find())
                matcher.appendReplacement(result, (toReply = vocabulary.get(matcher.group())) != null ? toReply : matcher.group());
            matcher.appendTail(result);
            return result.toString();
        }
    }
}
