package Pz4;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StudentUtils {
    static Map<String, Student> createMapFromList(List<Student> students){
        Map<String,Student> result  = new HashMap<>();
        for (Student ob:students) {
            result.put(ob.getFirstName().toLowerCase()+ob.getLastName().toLowerCase(), ob);
        }
        return result;
    }

    static void printStudents(List<Student> students, int course){
        Iterator<Student> iterator = students.iterator();
        while (iterator.hasNext()) {
            Student ob = iterator.next();
            if(ob.getCourse() == course)
                System.out.println(ob.toString());
        }
    }

    static List<Student> sortStudent(List<Student> students){
        Collections.sort(students, Comparator.comparing(Student::getFirstName));
        return students;
    }

    static Map<String, Integer> amountOfWords(String path) throws IOException {
        final String text = getTextFormURI(path);
        Pattern pattern = Pattern.compile("\\b[^ .,;\":?()!]+?\\b");
        Matcher matcher = pattern.matcher(text);
        Map<String, Integer> result = new HashMap<>();
        while (matcher.find()){
            if (result.containsKey(matcher.group())) result.put(matcher.group(), result.get(matcher.group())+1);
            else result.put(matcher.group(), 1);
        }
        return result;
    }
    static Map<String, Integer> amountOfWords(String path, Sort sort, Direction direction) throws IOException {
        List<Map.Entry<String, Integer>> list = new ArrayList<>(amountOfWords(path).entrySet());
        switch(sort){
            case WORDS:
                switch (direction){
                    case UP:
                        Collections.sort(list, (o2, o1)-> o1.getKey().compareTo(o2.getKey()));
                        break;
                    case DOWN:
                        Collections.sort(list, Comparator.comparing(Map.Entry::getKey));
                }
                break;
            case AMOUNT_OF_WORDS:
                switch (direction){
                    case UP:
                        Collections.sort(list, Comparator.comparing(Map.Entry::getValue));
                        break;
                    case DOWN:
                        Collections.sort(list, (o2, o1)-> o1.getValue().compareTo(o2.getValue()));
                }
        }
        Map<String, Integer> result = new LinkedHashMap<>();
        for (Map.Entry<String, Integer> i : list) result.put(i.getKey(),i.getValue());
        return result;
    }
    static String getTextFormURI(String path) throws IOException {
        BufferedReader br = new BufferedReader( new InputStreamReader(new FileInputStream(path)));
        String s = null;
        StringBuilder sb = new StringBuilder();
        while ((s = br.readLine()) != null) {
            sb.append(s).append(System.getProperty("line.separator"));
        }
        return sb.toString();
    }

    enum Sort{
        WORDS, AMOUNT_OF_WORDS
    }

    enum Direction {
        DOWN, UP
    }
}