package Pz4;

import lombok.Data;

import java.io.Serializable;

@Data
public class Student implements Serializable{
    private String firstName;
    private String lastName;

    public Student(String firstName, String lastNmae, int course) {
        this.firstName = firstName;
        this.lastName = lastNmae;
        this.course = course;
    }

    private int course;
}